#ifndef _LandedBook_h_
#define _LandedBook_h_
#pragma once

#include "Book.h"
#include "LibraryUser.h"

class LandedBook
{
public:
	LandedBook(Book* b, int bid, LibraryUser* u, int uid);
	LandedBook() {};

	inline Book* getBook() const { return _book; }
	inline void setBook(Book* value) { _book = value; }

	inline int getBookId() const { return _bookId; }
	inline void setBookId(int value) { _bookId = value; }

	inline LibraryUser* getUser() const { return _user; }
	inline void setUser(LibraryUser* value) { _user = value; }

	inline int getUserId() const { return _userId; }
	inline void setUserId(int value) { _userId = value; }
	
private:
	Book* _book;
	int _bookId;
	LibraryUser* _user;
	int _userId;
};

#endif _LandedBook_h_