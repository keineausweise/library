#ifndef _Book_h_
#define _Book_h_
#pragma once

#include<string>

using namespace std;

class Book
{
public:
	inline string getTitle() const { return _title; }
	inline void setTitle(const string& value) { _title = value; }

	inline string getAuthor() const { return _author; }
	inline void setAuthor(const string& value) { _author = value; }

private:
	string _title;
	string _author;
};

#endif //_Book_h_