#include "ConsoleMenu.h"
#include "ConsoleMenuItem.h"

HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
COORD CursorPosition; // used for goto

ConsoleMenu::ConsoleMenu()
{
	options = vector<ConsoleMenuItem*>(0);
	_selectedId = -1;
}

void ConsoleMenu::clearOptions(){
	options.clear();
	_selectedId = -1;
	_selectedString = "";
	_selectedKey = -1;
}

void ConsoleMenu::draw()
{
	gotoXY(0, 0);
	cout << " " << endl;
	for (int i = 0; i < options.size(); i++) {
		if (i == _selectedId) {
			SetConsoleTextAttribute(console, TEXT_BLACK_HIGHLIGHT_WHITE);
		}
		else {
			SetConsoleTextAttribute(console, TEXT_WHITE_HIGHLIGHT_NONE);
		}
		cout << options[i]->getName() << endl;
		SetConsoleTextAttribute(console, TEXT_WHITE_HIGHLIGHT_NONE);
	}
	cout << " " << endl;
}

void ConsoleMenu::addOption(const string& value, const int key)
{
	options.push_back(new ConsoleMenuItem(value, key));
}

void ConsoleMenu::selectOption(int optionId)
{
	_selectedId = optionId;
	if (_selectedId < 0) { _selectedId = options.size() - 1; }
	if (_selectedId > options.size() -1) { _selectedId = 0; }

	_selectedString = options[_selectedId]->getName();
	_selectedKey = options[_selectedId]->getKey();
}

void ConsoleMenu::gotoXY(int x, int y)
{
	CursorPosition.X = x;
	CursorPosition.Y = y;
	SetConsoleCursorPosition(console, CursorPosition);
}
