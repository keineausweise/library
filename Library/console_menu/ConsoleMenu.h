#ifndef _ConsoleMenu_h_
#define _ConsoleMenu_h_
#pragma once

#include<string>
#include<vector>
#include <iostream>

#include <windows.h>
#include "ConsoleMenuItem.h"


using namespace std;

class ConsoleMenu
{
public:
	ConsoleMenu();
	void draw();
	void clearOptions();
	void addOption(const string& value, const int key);
	void selectOption(int optionId);
	void gotoXY(int x, int y);
	inline string getSelectedString() const { return _selectedString; };
	inline int getSelectedId() const { return _selectedId; }
	inline int getSelectedKey() const { return _selectedKey; }

private:
	static const int TEXT_BLACK_HIGHLIGHT_WHITE = 240;
	static const int TEXT_WHITE_HIGHLIGHT_NONE = 15;
	int _selectedId;
	int _selectedKey;
	string _selectedString;
	vector<ConsoleMenuItem*> options;
};

#endif //_ConsoleMenu_h_