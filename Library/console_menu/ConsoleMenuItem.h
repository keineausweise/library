#ifndef _ConsoleMenuItem_h_
#define _ConsoleMenuItem_h_
#pragma once

#include<string>

//using namespace std;

class ConsoleMenuItem
{
public:
	ConsoleMenuItem(std::string name, int key)
		:_nameKey(name, key)
	{};
	inline std::string getName() const { return _nameKey.first; }
	inline int getKey() const { return _nameKey.second; }
private: 
	std::pair<std::string, int> _nameKey;
};

#endif //_ConsoleMenuItem_h_