#ifndef _Interaction_h_
#define _Interaction_h_
#pragma once

#include <iostream>
#include <conio.h>
#include <windows.h>
#include <cstdlib>

#include "./console_menu/ConsoleMenu.h"
#include "./actions/UserAddAction.h"
#include "./actions/ListUserAction.h"
#include "./actions/BookAddAction.h"
#include "./actions/BookListAction.h"
#include "./store/FileSaveLoad.h"
#include "./store/Store.h"

class Interaction
{
public:
	Interaction() :
		_userAddAction(),
		_listUserAction(),
		_bookAddAction(),
		_bookListAction()
	{};
	int listen();

private:
	void onAction(int);
	void onExitAction();
	static const int ACTION_ADD_USER = 10;
	static const int ACTION_LIST_USERS = 11;
	static const int ACTION_ADD_BOOK = 20;
	static const int ACTION_LIST_BOOKS = 21;
	static const int ACTION_SAVE_TO_FILE = 1000;
	static const int ACTION_LOAD_FROM_FILE = 2000;
	static const int ACTION_EXIT = 99;
	UserAddAction* _userAddAction;
	ListUserAction* _listUserAction;
	BookAddAction* _bookAddAction;
	BookListAction* _bookListAction;
};

#endif //_Interaction_h_
