#include "FileSaveLoad.h"

string FileSaveLoad::ENTRY_SPLITTER = "\n";

void FileSaveLoad::saveToFile()
{
	system("cls");
	std::cout << "������� ��� �����: ";
	std::string fileName;
	std::getline(std::cin, fileName);

	std::ofstream ofile;
	ofile.open(fileName);
	if (!ofile.is_open()){
		std::cout << "������ ������� ����. " << fileName;
		system("pause");
		return;
	}
	
	//__try{
		vector<LibraryUser*>& users = Store::getInstance()->getUsers();

		if (users.size() > 0){
			LibraryUser* current;
			for (vector<LibraryUser*>::iterator it = users.begin(); it != users.end(); ++it) {
				current = *it;
				ofile << ENTRY_KEY_USER << endl;
				ofile << current->getName() << endl;
				ofile << current->getSurname() << endl;
				ofile << ENTRY_SPLITTER;
				cout << "user " << current << endl;
			}
		}

		vector<Book*>& books = Store::getInstance()->getBooks();

		if (books.size() > 0){
			Book* current;
			for (vector<Book*>::iterator it = books.begin(); it != books.end(); ++it) {
				current = *it;
				ofile << ENTRY_KEY_BOOK << endl;
				ofile << current->getTitle() << endl;
				ofile << current->getAuthor() << endl;
				ofile << ENTRY_SPLITTER;
				cout << "book " << current << endl;
			}
		}

		vector<LandedBook*>& landed = Store::getInstance()->getLandedBooks();

		if (landed.size() > 0) {
			LandedBook* current;
			for (vector<LandedBook*>::iterator it = landed.begin(); it != landed.end(); ++it) {
				current = *it;
				ofile << ENTRY_KEY_LAND << endl;
				ofile << current->getBookId() << endl;
				ofile << current->getBook()->getTitle() << " " << current->getBook()->getAuthor() << endl;
				ofile << current->getUserId() << endl;
				ofile << current->getUser()->getName() << " " << current->getUser()->getSurname() << endl;
				ofile << ENTRY_SPLITTER;
				cout << "land " << current << endl;
			}
		}

		cout << "������";
		system("pause");

	//} 
	//__finally{
		ofile.close();
	//}

}

void FileSaveLoad::loadFromFile()
{
	system("cls");
	std::cout << "������� ��� �����: ";
	std::string fileName;
	std::getline(std::cin, fileName);

	std::ifstream ifile;
	ifile.open(fileName);

	if (!ifile.is_open()){
		cout << "������ ������� ����. " << fileName;
		system("pause");
		return;
	}

	int readState = READ_STATE_EMPTY_LINE,
		entryKey,
		entryReadLine;

	LibraryUser* temp_user;
	Book* temp_book;
	LandedBook* temp_land;

	Store* store = Store::getInstance();

	string line;
	while(ifile.good()){
		std::getline(ifile, line);

		switch (readState)
		{
			case READ_STATE_EMPTY_LINE:
			{
				if (!line.empty()) {
					entryKey = stoi(line);
					switch (entryKey) {
					case ENTRY_KEY_USER:
						readState = READ_STATE_USER;
						break;
					case ENTRY_KEY_BOOK:
						readState = READ_STATE_BOOK;
						break;
					case ENTRY_KEY_LAND:
						readState = READ_STATE_LAND;
						break;
					}
					if (readState != READ_STATE_EMPTY_LINE) {
						entryReadLine = 0;
					}
				}
				break;
			}

			case READ_STATE_USER:
			{
				if (line.empty()) {
					store->getUsers().push_back(temp_user);
					readState = READ_STATE_EMPTY_LINE;
				}

				switch (entryReadLine++)
				{
					case 0:
					{
						temp_user = new LibraryUser();
						temp_user->setName(line);
						break;
					}
					case 1:
					{
						temp_user->setSurname(line);
						break;
					}
				}
				break;
			}

			case READ_STATE_BOOK:
			{
				if (line.empty()) {
					store->getBooks().push_back(temp_book);
					readState = READ_STATE_EMPTY_LINE;
				}

				switch (entryReadLine++)
				{
					case 0:
					{
						temp_book = new Book();
						temp_book->setTitle(line);
						break;
					}
					case 1:
					{
						temp_book->setAuthor(line);
						break;
					}
				}
				break;
			}

			case READ_STATE_LAND:
			{
				if (line.empty()) {
					store->getLandedBooks().push_back(temp_land);
					readState = READ_STATE_EMPTY_LINE;
				}

				switch (entryReadLine++) {
					case 0:
					{
						int id = stoi(line);
						temp_land = new LandedBook();
						temp_land->setBookId(id);
						temp_land->setBook(store->getBooks()[id]);
						break;
					}
					case 2:
					{
						int id = stoi(line);
						temp_land->setUserId(id);
						temp_land->setUser(store->getUsers()[id]);
						break;
					}
				}
				break;//case READ_STATE_LAND
			}
		}

	}

}
