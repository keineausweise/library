#ifndef _FileSaveLoad_h_
#define _FileSaveLoad_h_
#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include "Store.h"

class FileSaveLoad
{
public:
	static void saveToFile();
	static void loadFromFile();

private:
	static const int ENTRY_KEY_USER = 100001;
	static const int ENTRY_KEY_BOOK = 100002;
	static const int ENTRY_KEY_LAND = 100100;
	static string ENTRY_SPLITTER;

	static const int READ_STATE_EMPTY_LINE = 0;
	static const int READ_STATE_USER = 1;
	static const int READ_STATE_BOOK = 2;
	static const int READ_STATE_LAND = 3;
};

#endif // _FileSaveLoad_h_