#ifndef _Store_h_
#define _Store_h_
#pragma once

#include <vector>
#include "../LibraryUser.h"
#include "../Book.h"
#include "../LandedBook.h"


class Store
{
public:
	Store(void):
		_users(),
		_books(){

	};
	inline vector<LibraryUser*>& getUsers(){return _users;}
	inline vector<Book*>& getBooks() {return _books;}
	inline vector<LandedBook*>& getLandedBooks() {return _landedBooks;}
	inline static Store* getInstance() { return _instance; }
	
private:
	vector<LibraryUser*> _users;
	vector<Book*> _books;
	vector<LandedBook*> _landedBooks;
	static Store* _instance;
};

#endif //_Store_h_
 