#include "Interaction.h"


using namespace std;

int Interaction::listen()
{
	bool running = true;
	ConsoleMenu* menu = new ConsoleMenu();
	menu->addOption("�������� ������������", ACTION_ADD_USER);
	menu->addOption("������ �������������", ACTION_LIST_USERS);
	menu->addOption("�������� �����", ACTION_ADD_BOOK);
	menu->addOption("������ ����", ACTION_LIST_BOOKS);
	menu->addOption("��������� � ����", ACTION_SAVE_TO_FILE);
	menu->addOption("��������� �� �����", ACTION_LOAD_FROM_FILE);
	menu->addOption("Exit", ACTION_EXIT);
	menu->selectOption(0);
	Store* storrage = Store::getInstance();

	while (running)
	{
		system("cls");
		printf(" �������������: [%d] ����: [%d] ���� �� �����: [%d]", 
			storrage->getUsers().size(),
			storrage->getBooks().size(),
			storrage->getLandedBooks().size());
		
		cout << endl << endl << endl;

		menu->draw();
		system("pause>nul");

		if (GetAsyncKeyState(VK_DOWN)) //down button pressed
		{
			menu->selectOption(menu->getSelectedId() + 1);
			continue;
		}

		if (GetAsyncKeyState(VK_UP) ) //up button pressed
		{
			menu->selectOption(menu->getSelectedId() - 1);
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN)) { // Enter key pressed
			onAction(menu->getSelectedKey());
		}

	}

	
	return 0;
}

void Interaction::onAction(int optionKey)
{	
	switch (optionKey)
	{
		case ACTION_ADD_USER:
		{
			_userAddAction->process();
			break;
		}
		case ACTION_LIST_USERS:
			{
				_listUserAction->process();
				break;
			}
		case ACTION_EXIT:
		{
			onExitAction();
			break;
		}
		case ACTION_ADD_BOOK:
		{
			_bookAddAction->process();
			break;
		}
		case ACTION_LIST_BOOKS:
		{
			_bookListAction->process();
			break;
		}
		case ACTION_SAVE_TO_FILE:
		{
			FileSaveLoad::saveToFile();
			break;
		}
		case ACTION_LOAD_FROM_FILE:
		{
			FileSaveLoad::loadFromFile();
			break;
		}
		default:
		{	
			system("cls");
			cout << "Not implemented" << endl;
			cin.get();
			break;
		}
	}
}

void Interaction::onExitAction() {
	exit(0);
}
