#ifndef _BookAddAction_h_
#define _BookAddAction_h_
#pragma once
#include "Action.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include <string>
#include <iostream>
#include <sstream>
class BookAddAction :
	public Action
{
public:
	int process();
};

#endif //_UserAddAction_h_