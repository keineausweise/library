#ifndef _BookListAction_h_
#define _BookListAction_h_
#pragma once
#include "Action.h"
#include "BookModAction.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include <string>
#include <iostream>
#include <sstream>

class BookListAction :
	public Action
{
public:
	int process();
};

#endif