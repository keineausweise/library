#ifndef _ListUserAction_h_
#define _ListUserAction_h_
#pragma once
#include "Action.h"
#include "ModUserAction.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include <string>
#include <iostream>
#include <sstream>

class ListUserAction :
	public Action
{
public:
	int process();

};

#endif