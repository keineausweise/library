#include "ListUserAction.h"

using namespace std;

int ListUserAction::process(){
	system("cls");
	
	bool running = true;
	ConsoleMenu* menu = new ConsoleMenu();
	menu->addOption("�����", -100);

	vector<LibraryUser*>& users = Store::getInstance()->getUsers();

	if (users.size() == 0){
		cout << "������������� ���.";
	}
	else {
		LibraryUser* current;
		int i = 0;
		for (vector<LibraryUser*>::iterator it = users.begin(); it != users.end(); ++it) {
			current = *it;
			menu->addOption(current->getName() + " " + current->getSurname(), i);
			i++;
		}
	}


	//menu->addOption("�������� ������������ " + name + " " + surname, 1);
	menu->selectOption(0);

	while (running)
	{
		system("cls");
		menu->draw();
		system("pause>nul");

		if (GetAsyncKeyState(VK_DOWN)) //down button pressed
		{
			menu->selectOption(menu->getSelectedId() + 1);
			continue;
		}

		if (GetAsyncKeyState(VK_UP) ) //up button pressed
		{
			menu->selectOption(menu->getSelectedId() - 1);
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN)) { // Enter key pressed
			int key = menu->getSelectedKey();
			if (key == -100) {
				delete menu;
				return 0;
			}
			LibraryUser* user  = Store::getInstance()->getUsers()[key];
			ModUserAction mua;// = new ModUserAction();
			mua.setId(key);
			mua.setUser(user);
			int result = mua.process();
			if (result == 0) {
				delete menu;
				return 0;
			}
		}

	}
	delete menu;
	return 0;
}
