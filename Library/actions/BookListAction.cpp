#include "BookListAction.h"

using namespace std;

int BookListAction::process() {
	system("cls");

	bool running = true;
	ConsoleMenu* menu = new ConsoleMenu();
	menu->addOption("�����", -100);
	menu->selectOption(0);

	vector<Book*>& books = Store::getInstance()->getBooks();

	if (books.size() == 0) {
		cout << "���� ���.";
	}
	else {
		Book* current;
		int i = 0;
		for (vector<Book*>::iterator it = books.begin(); it != books.end(); ++it) {
			current = *it;
			menu->addOption("\"" + current->getTitle() + "\" " + current->getAuthor(), i);
			i++;
		}
	}

	while (running)
	{
		system("cls");
		menu->draw();
		system("pause>nul");

		if (GetAsyncKeyState(VK_DOWN)) //down button pressed
		{
			menu->selectOption(menu->getSelectedId() + 1);
			continue;
		}

		if (GetAsyncKeyState(VK_UP)) //up button pressed
		{
			menu->selectOption(menu->getSelectedId() - 1);
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN)) { // Enter key pressed
			int key = menu->getSelectedKey();
			if (key == -100) {
				delete menu;
				return 0;
			}
			Book* book = Store::getInstance()->getBooks()[key];
			BookModAction mra;// = new ModUserAction();
			mra.setId(key);
			mra.setBook(book);
			int result = mra.process();
			if (result == 0) {
				delete menu;
				return 0;
			}
		}

	}
	delete menu;
	return 0;
}
