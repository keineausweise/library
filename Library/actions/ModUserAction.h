#ifndef _ModUserAction_h_
#define _ModUserAction_h_
#pragma once
#include "Action.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include <string>
#include <iostream>
#include <sstream>

class ModUserAction :
	public Action
{
public:
	ModUserAction(void) :
		_user(nullptr),
		_id(0)
	{};
	int process();

	inline LibraryUser* getUser() const {return _user;}
	inline void setUser(LibraryUser* value){ _user = value; }

	inline int getId() const {return _id;}
	inline void setId(int value) {_id = value;}

private:
	LibraryUser* _user;
	int _id;
};

#endif //_ModUserAction_h_