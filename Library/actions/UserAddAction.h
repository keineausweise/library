#ifndef _UserAddAction_h_
#define _UserAddAction_h_
#pragma once
#include "Action.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include <string>
#include <iostream>
#include <sstream>
class UserAddAction :
	public Action
{
public:
	int process();
};

#endif //_UserAddAction_h_