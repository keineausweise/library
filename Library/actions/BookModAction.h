#ifndef _BookModAction_h_
#define _BookModAction_h_
#pragma once
#include "Action.h"
#include "../console_menu/ConsoleMenu.h"
#include "../store/Store.h"
#include "../LandedBook.h"
#include <string>
#include <iostream>
#include <sstream>

class BookModAction :
	public Action
{
public:
	BookModAction(void) :
		_book(nullptr),
		_id(0)
	{};
	int process();

	inline Book* getBook() const { return _book; }
	inline void setBook(Book* value) { _book = value; }

	inline int getId() const { return _id; }
	inline void setId(int value) { _id = value; }

private:
	Book* _book;
	int _id;
	int _menuState;
	static const int STATE_SELECTING_BOOK = 0;
	static const int STATE_SELECTING_USER = 1;
};

#endif //_BookModAction_h_