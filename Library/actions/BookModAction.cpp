#include "BookModAction.h"

int BookModAction::process() {
	system("cls");

	string title = getBook()->getTitle(),
		author = getBook()->getAuthor();

	std::cout << title << " " << author << std::endl << std::endl << std::endl;

	_menuState = STATE_SELECTING_BOOK;
	Book* tempSelectedBook;
	int tempSelectedBookId;
	bool running = true;
	ConsoleMenu* menu = new ConsoleMenu();
	menu->addOption("�����", 1);
	menu->addOption("������ ������������", 2);
	menu->addOption("�������", 0);
	menu->selectOption(0);

	while (running)
	{
		system("cls");
		menu->draw();
		system("pause>nul");

		if (GetAsyncKeyState(VK_DOWN)) //down button pressed
		{
			menu->selectOption(menu->getSelectedId() + 1);
			continue;
		}

		if (GetAsyncKeyState(VK_UP)) //up button pressed
		{
			menu->selectOption(menu->getSelectedId() - 1);
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN)) { // Enter key pressed
			int key = menu->getSelectedKey();
			switch (_menuState)
			{
			case STATE_SELECTING_BOOK:
				{
					switch (key)
					{
					case 0:
						{
							vector<Book*>& books = Store::getInstance()->getBooks();
							books.erase(books.begin() + getId());
							delete getBook();
							return 0;
							break;
						}
					case 1:
						return 1;
						break;
					case 2:
						{
							tempSelectedBook  = Store::getInstance()->getBooks()[key];
							tempSelectedBookId = key;
							vector<LibraryUser*>& users = Store::getInstance()->getUsers();

							if (users.size() == 0){
								cout << "������������� ���.";
								system("pause");
								break;
							}
							menu->clearOptions();
							_menuState = STATE_SELECTING_USER;
							LibraryUser* current;
							int i = 0;
							for (vector<LibraryUser*>::iterator it = users.begin(); it != users.end(); ++it) {
								current = *it;
								menu->addOption(current->getName() + " " + current->getSurname(), i);
								i++;
							}
							menu->selectOption(0);
							break;	// case 2
						}
					}
					break; // case STATE_SELECTING_BOOK
				}
				case STATE_SELECTING_USER:
				{
					LibraryUser* user  = Store::getInstance()->getUsers()[key];
					LandedBook* landed = new LandedBook(tempSelectedBook, tempSelectedBookId, user, key);
					Store::getInstance()->getLandedBooks().push_back(landed);
					system("cls");
					cout << "����� ������." << endl;
					system("pause");
					return 1; // exiting bookmod
					break; // case STATE_SELECTING_USER
				}
			}
			

		}

	}

	return 0;
}
