
#include <iostream>
#include "LibraryUser.h"
#include "Book.h"
#include "Interaction.h"
#include "./store/Store.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);
	
	Interaction* i = new Interaction();
	i->listen();
		
	return 0;
}