#ifndef _LibraryUser_h_
#define _LibraryUser_h_
#pragma once

#include<string>

using namespace std;

class LibraryUser
{
public:
	inline string getName() const { return _name; }
	inline void setName(string value) { _name = value; }

	inline string getSurname() const { return _surname; }
	inline void setSurname(string value) { _surname = value; }

private:
	string _name; 
	string _surname;
};


#endif//_LibraryUser_h_ 